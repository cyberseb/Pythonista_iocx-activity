# Pythonista: iocx-activity.py

Pythonista script to pull instance activity from public mastodon api and send an iOS notification.

Pythonista: https://omz-software.com/pythonista/

Mastodon API: https://docs.joinmastodon.org/api/

You can schedule this to run daily/weekly by using iOS Shortcuts Automation.