# lib for API requests
import requests
# lib for iOS notifications
import notification

# get the data from the public API
req = requests.request('GET', 'https://ioc.exchange/api/v1/instance/activity')

# pull out current and last week
current_weekdata = req.json()[0]
last_weekdata = req.json()[1]

# format data for iOS notification
notification_string = current_weekdata['statuses'] + ' | ' + last_weekdata['statuses'] + ' statuses\n' + current_weekdata['logins'] + ' | ' + last_weekdata['logins'] + ' logins\n' + current_weekdata['registrations'] + ' | ' + last_weekdata['registrations'] + ' registrations'

# schedule the iOS notification with a delay of 0 seconds
notification.schedule(message=notification_string, delay=0, sound_name=None, action_url=None, title='IOC.exchange Statistics', subtitle='(current | last week)', attachments=None, trigger=None, actions=None, identifier=None)
